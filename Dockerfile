FROM ubuntu:18.04

RUN apt-get update && apt-get -y install \
cmake \
python3.8 \
python3-pip \
build-essential \
manpages-dev

RUN pip3 install conan

COPY . /home/assignment

WORKDIR /home/assignment

RUN conan create hello-world-lib-problem

WORKDIR hello-world-problem 

RUN mkdir build

WORKDIR build 

RUN conan install .. && cmake .. && make

ENTRYPOINT ["/bin/sh", "entrypoint.sh"]
